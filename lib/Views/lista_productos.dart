import 'package:barras/barras.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:front/Model/Producto.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

import 'package:front/Controller/api_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ListaProductos extends StatefulWidget {
  final String idAlacena;
  final String nombre;

  const ListaProductos({Key key, this.idAlacena, this.nombre})
      : super(key: key);

  @override
  _ListaProductosState createState() => _ListaProductosState();
}

class _ListaProductosState extends State<ListaProductos> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  DateTime _dateSelected = DateTime.now();
  TextEditingController vencimiento = TextEditingController();
  String nombre;
  String cantidad;
  String tipo;

  Future<String> _userId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var string = prefs.getString("UserId");
    return string;
  }

  TextInputType _keyboardType(String title) {
    switch (title) {
      case 'Cantidad':
        return TextInputType.datetime;
      default:
        return TextInputType.text;
    }
  }

  Future<void> _selectDate(
      BuildContext context, TextEditingController vencimiento) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _dateSelected,
        firstDate: new DateTime(2021),
        lastDate: new DateTime(2030));
    if (picked != null && picked != _dateSelected) {
      setState(() {
        _dateSelected = picked;
      });
      vencimiento.text = DateFormat('dd-MM-yyyy').format(_dateSelected);
    }
  }

  _esencialidad(String tipo) {
    if (tipo == "Esencial") {
      return Icon(
        Icons.star,
        color: Colors.grey,
        size: 15,
      );
    } else {
      return SizedBox();
    }
  }

  _dropDownForm(String title) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.blue)),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Flexible(
                child: DropdownButtonFormField<String>(
                  value: tipo,
                  items: <String>['No Esencial', 'Esencial']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                  onChanged: (value) {
                    setState(() {
                      tipo = value;
                    });
                  },
                  decoration: InputDecoration(
                    labelText: title,
                    border: InputBorder.none,
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Please enter $title Producto";
                    }
                    setState(() {
                      tipo = value;
                    });
                    return null;
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _dateForm(String title) {
    vencimiento.text = DateFormat('dd-MM-yyyy').format(_dateSelected);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: InkWell(
        onTap: () {
          _selectDate(context, vencimiento);
        },
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: Colors.blue)),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  child: TextFormField(
                    keyboardType: TextInputType.datetime,
                    controller: vencimiento,
                    onTap: () {
                      _selectDate(context, vencimiento);
                    },
                    showCursor: false,
                    decoration: InputDecoration(
                      labelText: title,
                      border: InputBorder.none,
                    ),
                    autocorrect: false,
                    textAlign: TextAlign.center,
                  ),
                ),
                Icon(Icons.arrow_drop_down,
                    color: Theme.of(context).brightness == Brightness.light
                        ? Colors.grey.shade700
                        : Colors.white70),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _textForm(String title) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.blue)),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: TextFormField(
            keyboardType: _keyboardType(title),
            decoration: InputDecoration(
              labelText: "$title producto",
              border: InputBorder.none,
            ),
            autocorrect: false,
            textAlign: TextAlign.center,
            validator: (value) {
              if (value.isEmpty) {
                return "Please enter $title Producto";
              }
              setState(() {
                switch (title) {
                  case 'Nombre':
                    nombre = value;
                    break;
                  case 'Cantidad':
                    cantidad = value;
                    break;
                  case 'Tipo':
                    tipo = value;
                    break;
                }
              });

              return null;
            },
          ),
        ),
      ),
    );
  }

  _customDialog(Widget body) {
    return Future.delayed(Duration.zero, () {
      showDialog(
          context: context,
          child: Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0)),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              child: Container(
                margin: EdgeInsets.only(left: 0.0, right: 0.0),
                child: Stack(
                  children: <Widget>[
                    Container(
                        padding: EdgeInsets.only(
                          top: 18.0,
                        ),
                        margin: EdgeInsets.only(top: 13.0, right: 8.0),
                        decoration: BoxDecoration(
                            color: Colors.teal,
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(16.0),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                color: Colors.black26,
                                blurRadius: 0.0,
                                offset: Offset(0.0, 0.0),
                              ),
                            ]),
                        child: body),
                    Positioned(
                      right: 0.0,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Align(
                          alignment: Alignment.topRight,
                          child: CircleAvatar(
                            radius: 19.0,
                            backgroundColor: Colors.white,
                            child: Icon(Icons.close, color: Colors.red),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )));
    });
  }

  Widget _addProductos() {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Center(
              child: Container(
                child: Text("Nuevo Producto",
                    style: TextStyle(fontSize: 20.0, color: Colors.white)),
              ),
            ),
            _textForm('Nombre'),
            _dateForm('Vencimiento'),
            _textForm('Cantidad'),
            _dropDownForm('Tipo'),
            SizedBox(height: 10.0),
            InkWell(
                child: Container(
                  padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(16.0),
                        bottomRight: Radius.circular(16.0)),
                  ),
                  child: Text(
                    "OK",
                    style: TextStyle(color: Colors.teal, fontSize: 25.0),
                    textAlign: TextAlign.center,
                  ),
                ),
                onTap: () async {
                  if (_formKey.currentState.validate()) {
                    print(vencimiento.text);
                    final response =
                        await Provider.of<ApiService>(context).postNewProducto({
                      'idAlacena': widget.idAlacena,
                      'nombre': nombre,
                      'vencimiento': vencimiento.text.toString(),
                      'cantidadDisponible': cantidad,
                      'tipo': tipo,
                      'alacena': true
                    });
                    // print(response.body);

                    if (response.body["status"] == "OK") {
                      setState(() {
                        Navigator.pop(context);
                      });
                    } else {
                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                          duration: Duration(seconds: 2),
                          backgroundColor: Colors.red,
                          content: Text("Something went wrong...")));
                    }
                  }
                })
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(widget.nombre),
      ),
      floatingActionButton: SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        overlayOpacity: 0.1,
        children: [
          SpeedDialChild(child: Icon(Icons.search)),
          SpeedDialChild(
              child: Icon(Icons.add),
              onTap: () {
                _customDialog(_addProductos());
              }),
          SpeedDialChild(
              child: Icon(Icons.qr_code_scanner),
              onTap: () async {
                final data = await Barras.scan(context);
              })
        ],
      ),
      body: FutureBuilder(
          future: _userId(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return FutureBuilder(
                future: Provider.of<ApiService>(context)
                    .getProductosAlacena(widget.idAlacena, snapshot.data),
                builder: (context, snapshot2) {
                  if (snapshot2.hasData) {
                    if (snapshot2.data.body['status'] == "OK") {
                      final List productos =
                          snapshot2.data.body['alacena']['productos'];
                      return _buildProductos(context, productos);
                    } else {
                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                          duration: Duration(seconds: 2),
                          backgroundColor: Colors.red,
                          content: Text("Something went wrong...")));
                    }
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
    );
  }

  ListView _buildProductos(BuildContext context, List productos) {
    return ListView.builder(
      itemCount: productos.length,
      padding: EdgeInsets.all(8),
      itemBuilder: (context, index) {
        Producto producto = Producto(
          productos[index]["idProducto"],
          productos[index]["nombreProducto"],
          productos[index]["vencimientoProducto"],
          int.parse(productos[index]["cantidadDisponible"]),
          productos[index]["tipoProducto"],
          productos[index]["alacena"],
        );
        return Card(
          elevation: 4,
          child: ListTile(
            subtitle: Text(producto.vencimiento.toString()),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  child: Row(
                    children: [
                      Text(
                        producto.nombre,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      _esencialidad(producto.tipo),
                    ],
                  ),
                ),
                Text(
                  producto.cantidad.toString(),
                  style: TextStyle(fontSize: 25),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
