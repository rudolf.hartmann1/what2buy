import 'package:flutter/material.dart';
import 'package:front/Views/home.dart';
import 'package:front/Views/lista_alacena.dart';
import 'package:front/Views/lista_compras.dart';
import 'package:front/Views/lista_super.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyDrawer extends StatefulWidget {
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  String switchName = "Home";

  _viewUpdate(String titulo) {
    setState(() {
      switchName = titulo;
    });
  }

  Future<String> _savedString(String item) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(item);
  }

  _viewSwitch(String n) {
    switch (n) {
      case "Home":
        return Home();
      case "Lista Alacena":
        return ListaAlacena();
      case "Lista Compras":
        return ListaCompras();
      case "Lista Supermercado":
        return ListaSuper();
      default:
        return Home();
    }
  }

  _drawerButton(IconData _icon, String titulo) {
    return ListTile(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Icon(_icon),
          Text(
            titulo,
            style: TextStyle(color: Colors.teal, fontSize: 20),
          ),
        ],
      ),
      onTap: () {
        _viewUpdate(titulo);
        Navigator.pop(context);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _viewSwitch(switchName),
      appBar: AppBar(
        title: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          Text(switchName),
        ]),
      ),
      drawer: Drawer(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            DrawerHeader(
              child: ListTile(
                onTap: () {},
                title: Container(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 100,
                        width: 100,
                        child: DecoratedBox(
                          position: DecorationPosition.foreground,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: ExactAssetImage('assets/UserImage.png')),
                          ),
                        ),
                      ),
                      FutureBuilder<Object>(
                          future: _savedString("UserName"),
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              return Text(snapshot.data,
                                  style: TextStyle(
                                      color: Colors.grey[600], fontSize: 20));
                            } else {
                              return SizedBox();
                            }
                          }),
                    ],
                  ),
                ),
              ),
            ),
            _drawerButton(Icons.home, "Home"),
            _drawerButton(Icons.fastfood, "Lista Alacena"),
            _drawerButton(Icons.add_shopping_cart, "Lista Compras"),
            _drawerButton(Icons.shopping_cart, "Lista Supermercado"),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: ListTile(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(
                        Icons.logout,
                        color: Colors.red,
                      ),
                      Text(
                        "Log Out",
                        style: TextStyle(color: Colors.red, fontSize: 20),
                      ),
                    ],
                  ),
                  onTap: () {
                    showDialog(
                        context: context,
                        child: AlertDialog(
                          title: Center(child: Text("Confirm to Log Out")),
                          content: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              OutlineButton.icon(
                                  onPressed: () => Navigator.pop(context),
                                  icon: Icon(
                                    Icons.clear,
                                    color: Colors.red,
                                  ),
                                  label: Text("")),
                              OutlineButton.icon(
                                  onPressed: () =>
                                      Navigator.pushNamedAndRemoveUntil(
                                          context,
                                          "/auth",
                                          (Route<dynamic> route) => false),
                                  icon: Icon(
                                    Icons.check,
                                    color: Colors.green,
                                  ),
                                  label: Text(""))
                            ],
                          ),
                        ));
                    // print("Log Out");
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
