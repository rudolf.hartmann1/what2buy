import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class ListaCompras extends StatefulWidget {
  @override
  _ListaComprasState createState() => _ListaComprasState();
}

class _ListaComprasState extends State<ListaCompras> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        overlayOpacity: 0.1,
        children: [
          SpeedDialChild(child: Icon(Icons.search)),
          SpeedDialChild(child: Icon(Icons.ac_unit))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(60.0),
        child: Container(
          decoration: BoxDecoration(color: Colors.deepPurple),
          child: ListView(),
        ),
      ),
    );
  }
}
