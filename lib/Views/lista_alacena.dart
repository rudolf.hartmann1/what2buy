import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:front/Model/Alacena.dart';
import 'package:front/Views/lista_productos.dart';
import 'package:provider/provider.dart';
import 'package:front/Controller/api_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ListaAlacena extends StatefulWidget {
  @override
  _ListaAlacenaState createState() => _ListaAlacenaState();
}

class _ListaAlacenaState extends State<ListaAlacena> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String nombre;

  Future<String> _userId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var string = prefs.getString("UserId");
    return string;
  }

  FutureBuilder _buildBody(BuildContext context) {
    return FutureBuilder(
        future: _userId(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return FutureBuilder(
              future:
                  Provider.of<ApiService>(context).getAlacenas(snapshot.data),
              builder: (context, snapshot2) {
                if (snapshot2.hasData) {
                  final List alacenas = snapshot2.data.body['alacenas'];
                  return _buildAlacenas(context, alacenas);
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }

  ListView _buildAlacenas(BuildContext context, List alacenas) {
    return ListView.builder(
      itemCount: alacenas.length,
      padding: EdgeInsets.all(8),
      itemBuilder: (context, index) {
        Alacena alacena = Alacena(alacenas[index]["idAlacena"],
            alacenas[index]["nombre"], alacenas[index]["productos"]);
        return Card(
          elevation: 4,
          child: ListTile(
            title: Text(
              alacena.nombre,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            onTap: () =>
                _navigateToAlacena(context, alacena.idAlacena, alacena.nombre),
            onLongPress: () {
              _customDialog(_changeAlacena(alacena));
            },
          ),
        );
      },
    );
  }

  void _navigateToAlacena(BuildContext context, String id, String nombre) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => ListaProductos(
          idAlacena: id,
          nombre: nombre,
        ),
      ),
    );
  }

  Widget _addAlacena() {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Center(
              child: Container(
                child: Text("Nueva Alacena",
                    style: TextStyle(fontSize: 20.0, color: Colors.white)),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.blue)),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    decoration: InputDecoration(
                      labelText: "Nombre Alacena",
                      border: InputBorder.none,
                    ),
                    autocorrect: false,
                    textAlign: TextAlign.center,
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Please enter Nombre Alacena";
                      }
                      setState(() {
                        nombre = value;
                      });
                      return null;
                    },
                  ),
                ),
              ),
            ),
            SizedBox(height: 10.0),
            InkWell(
              child: Container(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(16.0),
                      bottomRight: Radius.circular(16.0)),
                ),
                child: Text(
                  "OK",
                  style: TextStyle(color: Colors.teal, fontSize: 25.0),
                  textAlign: TextAlign.center,
                ),
              ),
              onTap: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                var userid = prefs.getString("UserId");
                if (_formKey.currentState.validate() && userid != null) {
                  final response =
                      await Provider.of<ApiService>(context).postNewAlacena({
                    'idUser': userid,
                    'nombre': nombre,
                  });
                  // print(response.body);

                  if (response.body["status"] == "OK") {
                    Navigator.pop(context);
                  } else {
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                        duration: Duration(seconds: 2),
                        backgroundColor: Colors.red,
                        content: Text("Something went wrong...")));
                  }
                }
              },
            )
          ],
        ),
      ),
    );
  }

  Widget _changeAlacena(Alacena alacena) {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Center(
              child: Container(
                child: Text("Nuevo Nombre",
                    style: TextStyle(fontSize: 20.0, color: Colors.white)),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.blue)),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    initialValue: alacena.nombre,
                    decoration: InputDecoration(
                      labelText: "Nombre Alacena",
                      border: InputBorder.none,
                    ),
                    autocorrect: false,
                    textAlign: TextAlign.center,
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Please enter Nombre Alacena";
                      }
                      setState(() {
                        nombre = value;
                      });
                      return null;
                    },
                  ),
                ),
              ),
            ),
            SizedBox(height: 10.0),
            InkWell(
              child: Container(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(16.0),
                      bottomRight: Radius.circular(16.0)),
                ),
                child: Text(
                  "OK",
                  style: TextStyle(color: Colors.teal, fontSize: 25.0),
                  textAlign: TextAlign.center,
                ),
              ),
              onTap: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                var userid = prefs.getString("UserId");
                if (_formKey.currentState.validate() && userid != null) {
                  final response =
                      await Provider.of<ApiService>(context).putAlacena(
                          alacena.idAlacena,
                          {
                            'nombre': nombre,
                          },
                          userid);
                  // print(response.body);

                  if (response.body["status"] == "OK") {
                    Navigator.pop(context);
                  } else {
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                        duration: Duration(seconds: 2),
                        backgroundColor: Colors.red,
                        content: Text("Something went wrong...")));
                  }
                }
              },
            )
          ],
        ),
      ),
    );
  }

  _customDialog(Widget body) {
    return Future.delayed(Duration.zero, () {
      showDialog(
          context: context,
          child: Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16.0)),
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              child: Container(
                margin: EdgeInsets.only(left: 0.0, right: 0.0),
                child: Stack(
                  children: <Widget>[
                    Container(
                        padding: EdgeInsets.only(
                          top: 18.0,
                        ),
                        margin: EdgeInsets.only(top: 13.0, right: 8.0),
                        decoration: BoxDecoration(
                            color: Colors.teal,
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(16.0),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                color: Colors.black26,
                                blurRadius: 0.0,
                                offset: Offset(0.0, 0.0),
                              ),
                            ]),
                        child: body),
                    Positioned(
                      right: 0.0,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Align(
                          alignment: Alignment.topRight,
                          child: CircleAvatar(
                            radius: 19.0,
                            backgroundColor: Colors.white,
                            child: Icon(Icons.close, color: Colors.red),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      floatingActionButton: SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        overlayOpacity: 0.1,
        children: [
          SpeedDialChild(child: Icon(Icons.search)),
          SpeedDialChild(
              child: Icon(Icons.add),
              onTap: () {
                _customDialog(_addAlacena());
              })
        ],
      ),
      body: Container(
          // decoration: BoxDecoration(color: Colors.deepPurple),
          child: _buildBody(context)),
    );
  }
}
