import 'package:flutter/material.dart';
import 'package:front/Controller/api_service.dart';
import 'package:front/Views/signUp.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String username;
  String password;
  bool _buttoncontroller = false;
  bool error = false;

  _errorText(bool update) {
    if (update == true)
      return Text(
        "Email or password wrong",
        style: TextStyle(color: Colors.red),
      );
    else
      return SizedBox();
  }

  _passwordVal(String title) {
    if (title == 'Password') return true;
    return false;
  }

  _textfield(String title, var type) {
    return Padding(
      padding: EdgeInsets.only(top: 10),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.blue)),
        child: Padding(
          child: TextFormField(
            obscureText: _passwordVal(title),
            decoration: InputDecoration(
              labelText: title,
              border: InputBorder.none,
            ),
            autocorrect: false,
            textAlign: TextAlign.center,
            validator: (value) {
              if (value.isEmpty) {
                return "Please enter $title";
              }
              switch (title) {
                case 'Email':
                  username = value;
                  break;
                case 'Password':
                  password = value;
                  break;
              }

              return null;
            },
          ),
          padding: EdgeInsets.symmetric(horizontal: 10),
        ),
      ),
    );
  }

  _buttonbuilder() {
    if (!_buttoncontroller) {
      return ElevatedButton(
        onPressed: () async {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          if (_formKey.currentState.validate()) {
            setState(() {
              _buttoncontroller = true;
            });

            final response = await Provider.of<ApiService>(context)
                .authUser({'userEmail': username, 'userPass': password});
            print(response.body);

            if (response.body["status"] == "OK") {
              setState(() {
                _buttoncontroller = false;
              });
              await prefs.setString("UserId", response.body["userId"]);
              await prefs.setString("UserName", response.body["userName"]);
              await prefs.setString(
                  "Birthdate", response.body["userFechaNacimiento"]);
              await prefs.setString("UserEmail", response.body["userEmail"]);

              FocusManager.instance.primaryFocus.unfocus();
              Navigator.pushNamedAndRemoveUntil(
                  context, "/home", (Route<dynamic> route) => false);
            } else if (response.body["message"] == "Datos no coincidentes") {
              setState(() {
                error = true;
                _buttoncontroller = false;
              });
            } else {
              setState(() {
                _buttoncontroller = false;
              });
              FocusManager.instance.primaryFocus.unfocus();
              _scaffoldKey.currentState.showSnackBar(SnackBar(
                  backgroundColor: Colors.red,
                  content: Text("Something went wrong...")));
            }
          }
        },
        child: Text("Sign In"),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: SizedBox(
          width: 30,
          height: 30,
          child: CircularProgressIndicator(),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Center(child: Text("Authentication")),
      ),
      body: ListView(
        padding: EdgeInsets.only(left: 100, right: 100),
        children: [
          SizedBox(
            height: 10,
          ),
          SizedBox(
            width: 100,
            height: 100,
            child: DecoratedBox(
                position: DecorationPosition.foreground,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: ExactAssetImage('assets/logo.png')))),
          ),
          Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                _errorText(error),
                _textfield('Email', username),
                _textfield('Password', password),
                _buttonbuilder(),
                Column(
                  children: [
                    Text(
                      "Or",
                      textAlign: TextAlign.center,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SignUpScreen()));
                      },
                      child: Text("Sign Up"),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
