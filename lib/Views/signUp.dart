import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:front/Controller/api_service.dart';
import 'package:front/Views/auth.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String username;
  String name;
  String email;
  String password;
  String cpassword;
  DateTime _dateSelected = DateTime.now();
  TextEditingController dateCtl = TextEditingController();
  bool _buttoncontroller = false;

  _passwordVal(String title) {
    if (title == 'Password' || title == 'Confirm Password') return true;
    return false;
  }

  TextInputType _keyboardType(String title) {
    switch (title) {
      case 'Birthdate':
        return TextInputType.datetime;
      case 'Email':
        return TextInputType.emailAddress;
      case 'Password':
        return TextInputType.visiblePassword;
      case 'Confirm Password':
        return TextInputType.visiblePassword;
      default:
        return TextInputType.text;
    }
  }

  Future<void> _selectDate(
      BuildContext context, TextEditingController dateCtl) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _dateSelected,
        firstDate: new DateTime(1900),
        lastDate: new DateTime(2030));
    if (picked != null && picked != _dateSelected) {
      setState(() {
        _dateSelected = picked;
      });

      dateCtl.text = DateFormat('dd-MM-yyyy').format(_dateSelected);
    }
  }

  _textfield(String title) {
    if (title == "Birthdate") {
      dateCtl.text = DateFormat('dd-MM-yyyy').format(_dateSelected);
      return Padding(
        padding: EdgeInsets.only(top: 10),
        child: InkWell(
          onTap: () {
            _selectDate(context, dateCtl);
          },
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: Colors.blue)),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Flexible(
                    child: TextFormField(
                      controller: dateCtl,
                      onTap: () {
                        _selectDate(context, dateCtl);
                      },
                      showCursor: false,
                      decoration: InputDecoration(
                        labelText: title,
                        border: InputBorder.none,
                      ),
                      autocorrect: false,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Icon(Icons.arrow_drop_down,
                      color: Theme.of(context).brightness == Brightness.light
                          ? Colors.grey.shade700
                          : Colors.white70),
                ],
              ),
            ),
          ),
        ),
      );
    } else {
      return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: Colors.blue)),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: TextFormField(
              keyboardType: _keyboardType(title),
              obscureText: _passwordVal(title),
              decoration: InputDecoration(
                labelText: title,
                border: InputBorder.none,
              ),
              autocorrect: false,
              textAlign: TextAlign.center,
              validator: (value) {
                if (value.isEmpty) {
                  return "Please enter $title";
                }
                switch (title) {
                  case 'Name':
                    name = value;
                    break;
                  case 'Email':
                    email = value;
                    break;
                  case 'Password':
                    password = value;
                    break;
                  case 'Confirm Password':
                    if (password != value) return "Passwords doesn't match";
                    break;
                }
                return null;
              },
            ),
          ),
        ),
      );
    }
  }

  _buttonbuilder() {
    if (!_buttoncontroller) {
      return ElevatedButton(
        onPressed: () async {
          if (_formKey.currentState.validate()) {
            setState(() {
              _buttoncontroller = true;
            });
            final response = await Provider.of<ApiService>(context)
                .postNewUser({
              'name': name,
              'date': dateCtl.text.toString(),
              'email': email,
              'pass': password
            });
            print(response.body);

            if (response.body["status"] == "OK") {
              setState(() {
                _buttoncontroller = false;
              });
              FocusManager.instance.primaryFocus.unfocus();
              _scaffoldKey.currentState.showSnackBar(SnackBar(
                  backgroundColor: Colors.green, content: Text("Success!")));
              await new Future.delayed(const Duration(seconds: 2));
              Navigator.pushNamedAndRemoveUntil(
                  context, "/auth", (Route<dynamic> route) => false);
            } else {
              setState(() {
                _buttoncontroller = false;
              });
              FocusManager.instance.primaryFocus.unfocus();
              _scaffoldKey.currentState.showSnackBar(SnackBar(
                  backgroundColor: Colors.red,
                  content: Text("Something went wrong...")));
            }
          }
        },
        child: Text("Sign Up"),
      );
    } else {
      return CircularProgressIndicator();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Align(alignment: Alignment.centerLeft, child: Text("Sign Up")),
      ),
      body: ListView(
        padding: EdgeInsets.only(
          left: 80,
          right: 80,
        ),
        children: [
          SizedBox(
            height: 10,
          ),
          SizedBox(
            width: 100,
            height: 100,
            child: DecoratedBox(
                position: DecorationPosition.foreground,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: ExactAssetImage('assets/logo.png')))),
          ),
          Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                _textfield("Name"),
                _textfield('Birthdate'),
                _textfield("Email"),
                _textfield('Password'),
                _textfield('Confirm Password'),
                _buttonbuilder()
              ],
            ),
          ),
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {},
      //   tooltip: 'Sign In',
      //   child: Text("Sign In"),
      // ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
