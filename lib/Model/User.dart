class User {
  int userId;
  String name;
  DateTime birthdate;
  String email;
  String password;

  User({this.userId, this.name, this.birthdate, this.email, this.password});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      userId: json['userId'],
      name: json['name'],
      birthdate: json['birthdate'],
      email: json['email'],
    );
  }
}
