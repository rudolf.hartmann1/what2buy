class Producto {
  String idProducto;
  String nombre;
  String vencimiento;
  int cantidad;
  String tipo;
  bool alacena;

  Producto(this.idProducto, this.nombre, this.vencimiento, this.cantidad,
      this.tipo, this.alacena);
}
