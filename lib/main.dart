import 'package:flutter/material.dart';
import 'package:front/Controller/api_service.dart';
import 'package:front/Views/drawer.dart';
import 'package:provider/provider.dart';
import 'package:front/Views/auth.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (_) => ApiService.create(),
      dispose: (_, ApiService service) => service.client.dispose(),
      child: MaterialApp(
        theme: ThemeData(
          primarySwatch: Colors.teal,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: AuthScreen(),
        routes: {
          "/auth": (_) => AuthScreen(),
          "/home": (_) => MyDrawer(),
        },
      ),
    );
  }
}
