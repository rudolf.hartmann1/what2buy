import 'package:chopper/chopper.dart';

part 'api_service.chopper.dart';

@ChopperApi()
abstract class ApiService extends ChopperService {
  @Post(path: '/user')
  Future<Response> postNewUser(
    @Body() Map<String, dynamic> body,
  );

  @Get(path: '/user')
  Future<Response> authUser(
    @Body() Map<String, dynamic> body,
  );

  @Post(path: '/alacena')
  Future<Response> postNewAlacena(
    @Body() Map<String, dynamic> body,
  );

  @Get(path: '/alacenas')
  Future<Response> getAlacenas(@Header('idUser') String idUser);

  @Get(path: '/alacena/{idAlacena}')
  Future<Response> getProductosAlacena(
      @Header('idAlacena') @Path('idAlacena') String idAlacena,
      @Header('idUser') String idUser);

  @Put(path: '/alacena/{idAlacena}')
  Future<Response> putAlacena(
    @Header('idAlacena') @Path('idAlacena') String idAlacena,
    @Body() Map<String, dynamic> body,
    @Header('idUser') String idUser,
  );

  @Post(path: '/alacena/producto')
  Future<Response> postNewProducto(
    @Body() Map<String, dynamic> body,
  );

  static ApiService create() {
    final client = ChopperClient(
      baseUrl: 'http://10.0.2.2:4000',
      services: [
        _$ApiService(),
      ],
      converter: JsonConverter(),
    );
    return _$ApiService(client);
  }
}
