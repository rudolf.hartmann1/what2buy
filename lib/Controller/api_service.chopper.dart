// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

class _$ApiService extends ApiService {
  _$ApiService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  final definitionType = ApiService;

  Future<Response> postNewUser(Map<String, dynamic> body) {
    final $url = '/user';
    final $body = body;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> authUser(Map<String, dynamic> body) {
    final $url = '/user';
    final $body = body;
    final $request = Request('GET', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> postNewAlacena(Map<String, dynamic> body) {
    final $url = '/alacena';
    final $body = body;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> getAlacenas(String idUser) {
    final $url = '/alacenas';
    final $headers = {'idUser': idUser};
    final $request = Request('GET', $url, client.baseUrl, headers: $headers);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> getProductosAlacena(String idAlacena, String idUser) {
    final $url = '/alacena/${idAlacena}';
    final $headers = {'idAlacena': idAlacena, 'idUser': idUser};
    final $request = Request('GET', $url, client.baseUrl, headers: $headers);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> putAlacena(
      String idAlacena, Map<String, dynamic> body, String idUser) {
    final $url = '/alacena/${idAlacena}';
    final $headers = {'idAlacena': idAlacena, 'idUser': idUser};
    final $body = body;
    final $request =
        Request('PUT', $url, client.baseUrl, body: $body, headers: $headers);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> postNewProducto(Map<String, dynamic> body) {
    final $url = '/alacena/producto';
    final $body = body;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }
}
